#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Start prompt in format: [short_day_name, day/month hour:minutes:seconds am/pm] user@host:cwd$
#if [[ ${EUID} == 0 ]] ; then
#	PS1='[\D{%a, %d/%m %I:%M:%S%P}] \[\033[01;31m\]\u\[\033[00m\]@\[\033[92m\]\h\[\033[00m\]:\[\033[94m\]\W\[\033[00m\]\$ '
#else
#	PS1='[\D{%a, %d/%m %I:%M:%S%P}] \[\033[01;92m\]\u\[\033[00m\]@\[\033[92m\]\h\[\033[00m\]:\[\033[94m\]\W\[\033[00m\]\$ '
#fi

# prompt
FMT_BOLD="\[\e[1m\]"
FMT_DIM="\[\e[2m\]"
FMT_RESET="\[\e[0m\]"
FMT_UNBOLD="\[\e[22m\]"
FMT_UNDIM="\[\e[22m\]"
FG_BLACK="\[\e[30m\]"
FG_BLUE="\[\e[34m\]"
FG_CYAN="\[\e[36m\]"
FG_GREEN="\[\e[32m\]"
FG_GREY="\[\e[37m\]"
FG_MAGENTA="\[\e[35m\]"
FG_RED="\[\e[31m\]"
FG_WHITE="\[\e[97m\]"
BG_BLACK="\[\e[40m\]"
BG_BLUE="\[\e[44m\]"
BG_CYAN="\[\e[46m\]"
BG_GREEN="\[\e[42m\]"
BG_MAGENTA="\[\e[45m\]"

function changes_in_branch() { 
    if [ -d .git ]; then
	if expr length + "$(git status -s)" 2>&1 > /dev/null; then     
	    echo -ne "\033[0;33m$(__git_ps1)\033[0m"; 
	else
	    echo -ne "\033[0;32m$(__git_ps1)\033[0m"; fi; 
    fi
}

PS1="\n ${FG_MAGENTA}╭─" # begin arrow to prompt
PS1+="${FG_MAGENTA}" # begin USERNAME container
PS1+="${BG_MAGENTA}${FG_CYAN}${FMT_BOLD}  " # print OS icon
PS1+="${FG_WHITE}\u" # print username
PS1+="${FMT_UNBOLD} ${FG_MAGENTA}${BG_BLUE} " # end USERNAME container / begin DIRECTORY container
PS1+="${FG_GREY}\w " # print directory
PS1+="${FG_BLUE}${BG_CYAN} " # end DIRECTORY container / begin FILES container
PS1+="${FG_WHITE}"
PS1+=" \$(find . -mindepth 1 -maxdepth 1 -type d | wc -l) " # print number of folders
PS1+=" \$(find . -mindepth 1 -maxdepth 1 -type f | wc -l) " # print number of files
PS1+=" \$(find . -mindepth 1 -maxdepth 1 -type l | wc -l) " # print number of symlinks
PS1+="${FMT_RESET}${FG_CYAN}"
PS1+="\$(git branch 2> /dev/null | grep '^*' | colrm 1 2 | xargs -I BRANCH echo -n \"" # check if git branch exists
PS1+="${BG_GREEN} " # end FILES container / begin BRANCH container
PS1+="${FG_WHITE} BRANCH " # print current git branch
PS1+="${FMT_RESET}${FG_GREEN}\")\n " # end last container (either FILES or BRANCH)
PS1+="${FG_MAGENTA}╰ " # end arrow to prompt
PS1+="${FG_CYAN}\\$ " # print prompt
PS1+="${FMT_RESET}"
export PS1


# BASH CONFIG
HISTSIZE=2048
HISTCONTROL=ignoreboth

# START SYSTEM INFO TOOL
neofetch

# ALIASES
alias ls='ls --color=auto -lh'
alias grep='grep --color=auto'
alias ip='ip --color=auto'
alias df='df -h'		        # human readable sizes
alias free='free -m'		        # size in MB
alias cal='cal -wm'                     # show week number + monday as first day of week
#alias lockd='light-locker-command -l'
alias suspd='systemctl suspend'
alias hibed='systemctl hibernate'
alias shutdown='shutdown now'
alias ~='cd ~'

# EXPORTS
export PATH=$PATH:/home/msk/.local/bin
