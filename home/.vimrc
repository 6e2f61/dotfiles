unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim
set number
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set hlsearch
set ignorecase
set smartcase
