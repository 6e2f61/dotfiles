**WIP document**

# PERSONALIZATION
- install pkgstats `sudo pacman -S pkgstats`

**installing the pkgstats package, which provides a timer that sends a list of the packages installed on your system, along with the architecture and the mirrors you use, to the Arch Linux developers in order to help them prioritize their efforts and make the distribution even better. The information is sent anonymously and cannot be used to identify you. You can view the collected data at the [Statistics page](https://pkgstats.archlinux.de/). More information is available in [this forum thread](https://bbs.archlinux.org/viewtopic.php?id=105431)**

- enable and start systemd-timesyncd service client for time sync `sudo systemctl enable systemd-timesyncd.service` and `sudo systemctl start systemd-timesyncd.service`
- edit `/etc/locale.conf` to setup regional formats
- install [mlocate](https://wiki.archlinux.org/index.php/Mlocate) and update its index database `sudo pacman -S mlocate` then `sudo updatedb`
- enable ssh `sudo systemctl start sshd`
- blacklist pcspeaker at boot creating `/etc/modprobe.d/nobeep.conf` 
- install yay [^1]

```
cd /opt
sudo git clone https://aur.archlinux.org/yay-git.git
sudo chown -R username:usergroup ./yay-git
cd yay-git
makepkg -si
yay --editmenu --nodiffmenu --save  #disable diff menu
```

- enable colored output `sudo vim /etc/pacman.conf` uncomment `#Color`
- install pacman-contrib package `sudo pacman -S pacman-contrib`
- enable and start `paccache.timer` to discard unused packages weekly

```
sudo systemctl enable paccache.timer
sudo systemctl start paccache.timer
```

- install xdg-user-dirs `sudo pacman -S xdg-user-dirs`
- edit/copy config file `~/.config/user-dirs.dirs` and `~/.config/user-dirs.locale`
- create directories `sudo xdg-user-dirs-update`
- install delta-git (coloring git diff output) `yay -S git-delta-bin`

## POWER MANAGEMENT
- install enable and start Intel thermald and cpupower

```
sudo pacman -S thermald cpupower
yay -S cpupower-gui
sudo systemctl enable thermald
sudo systemctl enable cpupower
sudo systemctl start thermald
sudo systemctl start cpupower
sudo systemctl disable cpupower-gui
```

### enable hibernation (on swap partition **not** in file)
- configure the initramfs `sudo vim /etc/mkinitcpio.conf`
When an initramfs with the base hook is used, which is the default, the resume hook is required in /etc/mkinitcpio.conf. Whether by label or by UUID, the swap partition is referred to with a udev device node, so the resume hook must go after the udev hook
`HOOKS=(base udev autodetect keyboard modconf block filesystems resume fsck)`
- regenerate initramfs `sudo mkinitcpio -P`
- get UUID of swap parition `lsblk -f | grep swap`
- edit kernel parameter (for GRUB) in `/etc/default/grub` add line `GRUB_CMDLINE_LINUX_DEFAULT="resume=UUID=uuid` where uuid is id of swap drive
- re-generate the `grub.cfg` file `sudo grub-mkconfig -o /boot/grub/grub.cfg`

## Network
- install networkmanager addons `sudo pacman -S networkmanager-openvpn nm-connection-editor  network-manager-applet`
- install samba support `sudo pacman -S samba`
- install NordVPN client `yay -S nordvpn-bin`
- add user  to nordvpn group `sudo gpasswd -a <username> nordvpn`
- enable and start nordvpn.service `sudo systemctl enable nordvpnd.service && sudo systemctl start nordvpnd.service`
- login to nordvpn service using credentials `sudo nordvpn login`
- enable NordLynx technology `sudo nordvpn set technology nordlynx`


## X11 & DISPLAY DRIVERS
- install xorg `sudo pacman -S xorg`
- list graphic adapters to verify which drivers to install `lspci | grep -e VGA -e 3D`
- for NVIDIA: install `sudo pacman -S nvidia-lts nvidia nvidia-settings`
- for INTEL: install `sudo pacman -S mesa`
- generate Xorg config file `sudo nvidia-xconfig`
- use nvidia-settings to review avaiable options `nvidia-settings`
- enable transparency installing compositor `sudo pacman -S picom`

## Fonts
- install additional fonts `sudo pacman -S ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-liberation ttf-linux-libertine ttf-opensans cantarell-fonts`
- install aur windows crisp fonts `yay -S ttf-ttf-dmcasansserif font-manager ttf-ms-win10 nerd-fonts-dejavu-complete`
	- download windows image `https://www.microsoft.com/en-us/software-download/windows10ISO`
	- extract windows fonts `7z e install.wim 1/Windows/{Fonts/"*".{ttf,ttc},System32/Licenses/neutral/"*"/"*"/license.rtf} -ofonts/`
	- move all fonts to directory `~/.cache/yay/ttf-ms-win10/`- where file PKGBUILD is located
- edit/copy config file `~/.config/fontconfig/fonts.conf`

## DM
- install display manager and greeter `sudo pacman -S lightdm lightdm-webkit2-greeter light-locker`
- enable display manager service to autostart on logon `sudo systemctl enable lightdm`
- edit/copy display manager greeter file `/etc/lightdm/lightdm.conf`
- edit/copy greeter startup script `/etc/lightdm/greeter_setup.sh`
- make it executable `sudo chmod +x /etc/lightdm/greeter_setup.sh`
- install greeter theme `yay -S lightdm-webkit-theme-aether`
- add theme to webkit2 config `sudo vim /etc/lightdm/lightdm-webkit2-greeter.conf`

```
[greeter]
webkit_theme = lightdm-webkit-theme-aether
```

- restart display manager `sudo systemctl restart display-manager`

## DE & WM
- install dekstop envirnoment `sudo pacman -S xfce4 xfce4-goodies i3-wm i3status dmenu rofi py3status`
- install arc-theme `sudo pacman -S arc-solid-gtk-theme arc-gtk-theme gtk-engine-murrine`
- install arc-icon, moka-icon and gnome-icons-theme theme `yay -S arc-icon-theme-full-git moka-icon-theme-full-git`
- edit/copy config files `~/.config/i3` and `~/.config/i3status`
- edit/copy config file `~/.config/rofi/config.rasi` *dmenu replacement*

## GTK & QT
- install qt5 config tool for qt5 style `sudo pacman -S qt5ct`
- add variable `QT_QPA_PLATFORMTHEME=qt5ct` to `~/.pam_environment`
- install qt5-styleplugins to use gtk themes `yay -S qt5-styleplugins`
- install kvantum `sudo pacman -S kvantum-qt5` 
- edit/copy config files:`~/.gtkrc-2.0` and `~/.config/gtk-3.0/settings.ini`

Open the file-chooser within the current working directory and not the recent location

- gtk2: add `StartupMode=cwd` to file`~/.config/gtk-2.0/gtkfilechooser.ini`
- gtk3: execute `gsettings set org.gtk.Settings.FileChooser startup-mode cwd`
- install xsettingsd `sudo pacman -S xsettingsd` *for transparency*
- edit/copy config file `~/.config/xsettingsd/xsettingsd.conf`

## Mouse
- install Logiops to use with MX mouse `yay -S logiops-git solaar-git`
- edit/copy config file `/etc/logid.cfg`
- edit/copy config file `~/.config/solaar/config.json`
- enable `sudo systemctl enable logid` and start service `sudo systemctl start logid`

## Audio & Video
- install additional plugins `sudo pacman -S alsa-utils alsa-plugins`
- list modules using `cat /proc/asound/modules` and edit `/etc/modprobe.d/alsa-base.conf` setting fixed soundcard order with
```
options snd_virtuoso index=0
options snd_hda_intel index=1
```
- install pulseaudio server `sudo pacman -S pulseaudio pulseaudio-alsa pavucontrol pasystray` for x32 (ie. Steam) `lib32-libpulse lib32-alsa-plugins`
- install codecs `sudo pacman -S lame flac gst-libav`
- install deadbeef
- install playerctl # control media player via keyboard multimedia keys
- install mpv + edit/copy config `~/.config/mpv`
- install napiprojekt `yay -S napi-bash`
- install simplescreenrecoder

## Firewall
- install firewalld

## printers (multidevice)
- install `cups cups-pdf`
- install `system-config-printer`
- install `hplip` -  	Drivers for HP DeskJet, OfficeJet, Photosmart, Business Inkjet and some LaserJet
- enable socket `sudo systemctl enable cups.socket`  systemd will not start CUPS immediately; it will just listen to the appropriate sockets. Then, whenever a program attempts to connect to one of these CUPS sockets, systemd will start cups.service and transparently hand over control of these ports to the CUPS process. 
- install `sane xsane`


## additional apps
- install neofetch 
- install workrave
- install redshift + edit/copy config `~/.config/redshift.conf`
- install cryptomator
- install libreoffice-fresh + enable backup file + change default font to Arial for Calc `F11 > Cell Styles > Default` modify style , next `Shift + F11` name it and mark as default template
- install ghostwriter
- install pdf viewver/editor `yay -S masterpdfeditor`
- install dictionary systemwide `sudo pacman -S hunspell-pl hunspell-en_GB hunspell-en_US`
- install doublecmd + edit/copy config `~/.config/doublecmd/*`
- install android-file-transfer
- install archive tools `sudo pacman -S p7zip unrar xarchiver`
- install claws-mail + edit/copy config `~/.claws-mail`
- install openvpn
- install calibre
- install galculator
- install signal-desktop
- install dunst (notification) # systemctl --user 
- install joplin `yay -S joplin-desktop` + dropbox sync support
- install teamviewer `yay -S teamviewer`
- install tixati `yay -S tixati`
- install authy `yay -S authy`
- install docker `sudo pacman -S docker docker-compose`
- maps management `sudo pacman -S qmapshack` + config
- install *better* top `sudo pacman -S htop`
- install unison for 2way backup/sync solution

## Gnucash
- install gnucash
- configure Automatic Retrieval of Quotes installing Finance::Quote `sudo /usr/bin/gnc-fq-update` or `sudo pacman -S perl-finance-quote`
- edit reports `~/.local/share/gnucash/saved-reports-2.8` + config file `~/.local/share/gnucash/filename.gnucash`

## KeepassXC
- install keepassxc
- configure ssh-agent with systemd user `~/.config/systemd/user/ssh-agent.service`
- add `SSH_AUTH_SOCK DEFAULT="${XDG_RUNTIME_DIR}/ssh-agent.socket"` to `~/.pam_environment`
- start ssh-agent
```
systemctl --user enable ssh-agent
systemctl --user start ssh-agent
```
- enable lingering `loginctl enable-linger $(whoami)`
- enable SSH AGENT support in KeepassXC options
- re-logon

## Gnome-keyring
- instlall packages `sudo pacman -S gnome-keyring libsecret seahorse`

## Firefox 
- install firefox + ankerfox + user-overrides.js [12bytes](https://12bytes.org/articles/tech/firefox/firefoxgecko-configuration-guide-for-privacy-and-performance-buffs/)
- remove xpi addons `cd /usr/lib/firefox/browser/features/ & sudo rm *.xpi`
- add line to pacman.conf `NoExtract = /usr/lib/firefox/browser/features/*`

## Browsers
- install brave
- install chrome-google
- install download manager `sudo pacman -S persepolis`

## Virtualbox
- install virtualbox for lts-kernel `sudo pacman -S virtualbox  virtualbox-guest-iso virtualbox-host-dkms linux-lts-headers `
- to use the USB ports of host machine in  virtual machines, add user to the `vboxusers` user group `sudo gpasswd -a user vboxusers`
- install Oracle Extension Pack `yay -S  virtualbox-ext-oracle`

## Images&Photos
- install feh + edit config file `~/.config/.fehbg`
- install geeqie or nomacs
- install flameshot
- install gimp
- install darktable and opencl+nvidia
- ~~install dispcalGUI~~
- install rapid-photo-downloader
- install hugin
- install digiKam + breeze-icons (Adwaita icon pack missing some digiKam icons - then in digiKam Settings -> Configure digiKam -> Miscellaneous -> Appearance -> Icon theme and select "Breeze")

## dev
- install code `sudo pacman -S code` + add marketplace support:
- install `yay -S code-marketplace` and `yay -S code-features`
- install pycharm-community `sudo pacman -S pycharm-community-edition`


## mimetypes
- deprecated but several applications still read/write to `~/.local/share/applications/mimeapps.list`, make symlink `ln -s ~/.config/mimeapps.list ~/.local/share/applications/mimeapps.list`
- for simple assigning default apps use `yay -S selectdefaultapplication-git`


## misc
- generate lm_sensors config `sudo sensors-detect`
- install dconf `sudo pacman -S dconf dconf-editor`
	- export settings `dconf dump / > ~/.config/dconf-export`
	- restore settings `dconf load / < ~/.config/dconf-export`


## config files list

```
TBD
```

# extra sources
[Arch Linux SSH Server Setup, Customization and Optimization](https://linuxhint.com/arch_linux_ssh_server/)

[^1]: https://www.tecmint.com/install-yay-aur-helper-in-arch-linux-and-manjaro/
